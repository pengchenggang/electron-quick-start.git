// Modules to control application life and create native browser window
const { app, BrowserWindow, BrowserView } = require('electron')
const path = require('path')
let t0
console.info('start', new Date())

function createWindow () {
  const t1 = new Date()
  console.info('createWindow', t1, t0, t0 - t1)
  // Create the browser window.
  // console.info('createWindow slow!!!')

  // win = new BrowserWindow({
  //   // 隐藏窗口
  //   show: true,
  //   // 背景透明
  //   transparent: false,
  //   // mac标题栏
  //   titleBarStyle: 'hiddenInset',
  //   // 隐藏标题栏
  //   frame: false,
  // })
  // 创建View遮罩
  // const view = new BrowserView({})                   
  // win.setBrowserView(view)
  // view.setBounds({ x: 0, y: 0, width: 1050, height: 700 })
  // if (process.env.WEBPACK_DEV_SERVER_URL) {
  //   win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
  //   view.webContents.loadURL(process.env.WEBPACK_DEV_SERVER_URL + '/loading.html')
  //   if (!process.env.IS_TEST) win.webContents.openDevTools()
  // } else {
  //   createProtocol('app')
  //   view.webContents.loadURL('app://./loading.html')
  //   win.loadURL('app://./index.html')
  // }
  // view.webContents.loadFile('index.html')
  // win.loadFile('index.html')
  // // 显示窗口
  // view.webContents.on('dom-ready', () => {
  //   console.log('dom-ready', new Date())
  //   win.show()
  // })
  // 关闭遮罩
  // ipcMain.on('stop-loading-main', () => {
  //   win.removeBrowserView(view)
  // })


  const mainWindow = new BrowserWindow({
    backgroundColor: '#2e2c29',
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      preload: path.join(__dirname, 'preload.js')
    },
    show: false,
  })
  // mainWindow.loadURL('http://www.baidu.com/')
  mainWindow.loadFile('./index.html')
  // mainWindow.loadFile(`file://${__dirname}/index.html`)
  // mainWindow.on('ready-to-show', function () {
  //   mainWindow.show() // 初始化后再显示
  // })
  mainWindow.once('ready-to-show', function () {
    console.info('once', new Date() - t1)
    mainWindow.show() // 初始化后再显示
  })

  // mainWindow = new BrowserWindow({
  //   height: 600,
  //   width: 960,
  //   frame: false,
  //   minWidth: 710,
  //   minHeight: 500,
  //   offscreen: true,
  //   webPreferences: { webSecurity: false },
  //   resizable: true,
  //   skipTaskbar: false,
  //   flashFrame: true,
  //   show: false // newBrowserWindow创建后先隐藏
  // })
  // // mainWindow.openDevTools() // 开发者工具
  // // mainWindow.loadURL(winURL)
  // mainWindow.loadFile('index.html')
  // mainWindow.on('ready-to-show', function () {
  //   mainWindow.show() // 初始化后再显示
  // })

  // and load the index.html of the app.

  // console.info('createWindow slow!!!')

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  t0 = new Date()
  console.info('whenReady', t0)
  createWindow()

  app.on('activate', function () {
    console.info('activate')
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  console.info('window-all-closed')
  if (process.platform !== 'darwin') app.quit()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
