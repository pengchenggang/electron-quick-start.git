// Modules to control application life and create native browser window
// mainWindow = new BrowserWindow 打开慢的原因 electron 已解决 Windows Defender 拦截导致
// https://www.cnblogs.com/pengchenggang/p/15879283.html
const { app, BrowserWindow } = require('electron')
const path = require('path')

function createWindow () {
  const t1 = new Date()
  const mainWindow = new BrowserWindow({
    // backgroundColor: '#2e2c29',
    alwaysOnTop: true,
    width: 300,
    height: 150,
    webPreferences: {
      nodeIntegration: true,
      preload: path.join(__dirname, 'preload.js')
    },
    show: true,
    frame: false,
    transparent: true,
  })
  mainWindow.loadFile('index.html')
  mainWindow.once('ready-to-show', function () {
    console.info('once', new Date() - t1)
    mainWindow.show() // 初始化后再显示
    // mainWindow.webContents.openDevTools()
  })
}

app.whenReady().then(() => {
  t0 = new Date()
  console.info('whenReady', t0)
  createWindow()

  app.on('activate', function () {
    console.info('activate')
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

app.on('window-all-closed', function () {
  console.info('window-all-closed')
  if (process.platform !== 'darwin') app.quit()
})
